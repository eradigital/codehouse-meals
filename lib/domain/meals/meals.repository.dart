import 'dart:convert';

import 'package:eradigital/app/modules/widgets/dialogs/custom_dialog.widget.dart';
import 'package:eradigital/config.dart';
import 'package:eradigital/core/utils/snackbar.util.dart';
import 'package:eradigital/infrastructure/meals/meals.service.dart';
import 'package:eradigital/models/meals/meals_response.model.dart';
import 'package:get/get.dart';

class MealsRepository {
  final MealsService api = MealsService();
  final env = ConfigEnvironments.getEnvironments()['env'];
  late MealsResponse response;
  
  Future<MealsResponse> getAll(cat) async {
    var result = await api.getAll(cat);

    result.fold(
      (l) {
        if (env != Environments.PRODUCTION)
          Get.dialog(
            CustomDialogBox(
              descriptions: l,
              title: 'Info',
              text: 'OK',
            ),
          );
      },
      (r) {
        response = MealsResponse.fromJson(json.decode(r.toString()));
      },
    );
    
    return response;
  }

  Future<MealsResponse> getId(id) async {
    var result = await api.getId(id);

    result.fold(
      (l) {
        if (env != Environments.PRODUCTION)
          Get.dialog(
            CustomDialogBox(
              descriptions: l,
              title: 'Info',
              text: 'OK',
            ),
          );
      },
      (r) {
        response = MealsResponse.fromJson(json.decode(r.toString()));
      },
    );

    return response;
  }

  Future<MealsResponse> findByName(keyword) async {
    var result = await api.findByName(keyword);

    result.fold(
      (l) {
        if (env != Environments.PRODUCTION)
          Get.dialog(
            CustomDialogBox(
              descriptions: l,
              title: 'Info',
              text: 'OK',
            ),
          );
      },
      (r) {
        if (json.decode(r.toString())['meals'] == null) {
          return SnackbarUtil.showInfo(
              message: 'Meals not found, please search with any keyword');
        }
        response = MealsResponse.fromJson(json.decode(r.toString()));
      },
    );

    return response;
  }

  // Future<MealsResponse> edit(data) async {
  //   // return api.edit(obj);
  // }

  // Future<MealsResponse> add(data) async {
  //   // return api.add(data);
  // }
}
