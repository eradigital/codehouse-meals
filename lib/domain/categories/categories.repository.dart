import 'dart:convert';

import 'package:eradigital/app/modules/widgets/dialogs/custom_dialog.widget.dart';
import 'package:eradigital/config.dart';
import 'package:eradigital/infrastructure/categories/categories.service.dart';
import 'package:eradigital/models/categories/categories_response.model.dart';
import 'package:get/get.dart';

class CategoriesRepository {
  final CategoriesService api = CategoriesService();
  final env = ConfigEnvironments.getEnvironments()['env'];
  late CategoriesResponse response;

  Future<CategoriesResponse> getAll() async {
    var result = await api.getAll();

    result.fold(
      (l) {
        if (env != Environments.PRODUCTION)
          Get.dialog(
            CustomDialogBox(
              descriptions: l,
              title: 'Info',
              text: 'OK',
            ),
          );
      },
      (r) {
        response = CategoriesResponse.fromJson(json.decode(r.toString()));
      },
    );

    return response;
  }

  // Future<CategoriesResponse> getId() async {
  //   // return api.getId(id);
  // }

  // Future<CategoriesResponse> delete(id) async {
  //   // return api.delete(id);
  // }

  // Future<CategoriesResponse> edit(data) async {
  //   // return api.edit(obj);
  // }

  // Future<CategoriesResponse> add(data) async {
  //   // return api.add(data);
  // }
}
