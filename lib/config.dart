class Environments {
  static const String PRODUCTION = 'prod';
  static const String DEV = 'dev';
  static const String LOCAL = 'local';
}

class ConfigEnvironments {
  static const String _currentEnvironments = Environments.PRODUCTION;
  static const List<Map<String, String>> _availableEnvironments = [
    {
      'env': Environments.LOCAL,
      'url': 'https://www.themealdb.com/api/json/v1/1',
    },
    {
      'env': Environments.DEV,
      'url': 'https://www.themealdb.com/api/json/v1/1',
    },
    {
      'env': Environments.PRODUCTION,
      'url': 'https://www.themealdb.com/api/json/v1/1',
    },
  ];

  static Map<String, String> getEnvironments() {
    return _availableEnvironments.firstWhere(
      (d) => d['env'] == _currentEnvironments,
    );
  }
}
