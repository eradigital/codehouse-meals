import 'package:eradigital/app/modules/onboarding/controllers/onboarding_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intro_slider/intro_slider.dart';

class OnboardingView extends GetView<OnboardingController> {
  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: controller.slides,
      onDonePress: controller.onDonePress,
      renderNextBtn: const Text('Next'),
      renderPrevBtn: const Text('Prev'),
      renderSkipBtn: const Text('Skip'),
      renderDoneBtn: const Text('Done'),
    );
  }
}
