import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intro_slider/slide_object.dart';

class OnboardingController extends GetxController {
  List<Slide> slides = [];

  @override
  void onInit() {
    super.onInit();

    slides.add(
      new Slide(
        title: "FOOD",
        styleTitle: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        styleDescription: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        description: "MANY DIFFERENT FOOD VARIATIONS TO FOLLOW YOUR TASTE",
        pathImage: "assets/images/ob-001.png",
        backgroundColor: Colors.white,
      ),
    );
    slides.add(
      new Slide(
        title: "EASY",
        styleTitle: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        styleDescription: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        description:
            "YOU DON'T NEED TO FURTHER COOKING YOURSELF, JUST CHOOSE THE FOOD YOU LIKE",
        pathImage: "assets/images/ob-002.png",
        backgroundColor: Colors.white,
        
      ),
    );
    slides.add(
      new Slide(
        title: "INSTAN",
        styleTitle: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        styleDescription: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        description: "YOU CAN ORDER YOUR FOOD ANYTIME AND ANYWHERE",
        pathImage: "assets/images/ob-003.png",
        backgroundColor: Colors.white,
      ),
    );
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void onDonePress() {
    Get.toNamed('/login');
  }
}
