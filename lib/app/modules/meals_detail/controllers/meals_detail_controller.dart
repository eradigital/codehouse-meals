import 'package:eradigital/domain/meals/meals.repository.dart';
import 'package:eradigital/models/meals/meals_response.model.dart';
import 'package:get/get.dart';

class MealsDetailController extends GetxController {
  final MealsRepository meals = MealsRepository();
  final detailMeals = <dynamic>[].obs;
  final isLoadingMealsDetail = false.obs;
  
  @override
  void onInit() {
    super.onInit();
  }
 
  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> getDetailMeals(id) async {
    isLoadingMealsDetail.value = true;
    MealsResponse result = await meals.getId(id);
    detailMeals.assignAll(result.meals);
    isLoadingMealsDetail.value = false;
  }
}
