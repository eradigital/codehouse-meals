import 'package:get/get.dart';

import '../controllers/meals_detail_controller.dart';

class MealsDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MealsDetailController>(
      () => MealsDetailController(),
    );
  }
}
