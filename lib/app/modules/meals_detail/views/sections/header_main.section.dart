import 'package:eradigital/app/modules/meals_detail/controllers/meals_detail_controller.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HeadSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MealsDetailController controller = Get.find<MealsDetailController>();
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Obx(
          () {
            return controller.isLoadingMealsDetail.value
                ? Center(
                    child: Container(
                      height: 30,
                      width: 30,
                      margin: EdgeInsets.only(bottom: 60),
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.detailMeals.length > 0
                            ? controller.detailMeals[0]['strMeal']
                            : '',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: CustomFontStyle().headingOne(Colors.white),
                      ),
                      Container(
                        width: Get.width - 220,
                        child: Text(
                          'Category: ' +
                              (controller.detailMeals.length > 0
                                  ? controller.detailMeals[0]['strCategory']
                                  : ''),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: CustomFontStyle().textOne(Colors.white),
                        ),
                      ),
                    ],
                  );
          },
        ),
      ),
    );
  }
}
