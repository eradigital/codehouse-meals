import 'package:eradigital/app/modules/meals_detail/controllers/meals_detail_controller.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContentSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MealsDetailController controller = Get.find<MealsDetailController>();
    return Container(
      height: Get.height * 0.5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  'INSTRUCTIONS :',
                  style: CustomFontStyle().headingFour(Colors.black),
                ),
                SizedBox(
                  height: 20,
                ),
                Obx(
                  () => controller.isLoadingMealsDetail.value
                      ? Center(
                          child: Container(
                            height: 30,
                            width: 30,
                            margin: EdgeInsets.only(top: 30),
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : Text(
                          controller.detailMeals.length > 0
                              ? controller.detailMeals[0]['strInstructions']
                              : '',
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
