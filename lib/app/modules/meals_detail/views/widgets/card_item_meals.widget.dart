import 'package:cached_network_image/cached_network_image.dart';
import 'package:eradigital/app/modules/widgets/containers/shadow.widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CardImageMeals extends StatelessWidget {
  const CardImageMeals({
    Key? key,
    required this.params,
  }) : super(key: key);

  final params;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: Get.height * 0.5 - 50,
      right: 40,
      child: ShadowContainer(
        height: 160,
        width: 160,
        child: Hero(
          tag: params['id'],
          child: CachedNetworkImage(
            imageUrl: params['image'],
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                CircularProgressIndicator(value: downloadProgress.progress),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      ),
    );
  }
}