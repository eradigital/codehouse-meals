import 'package:eradigital/app/modules/meals_detail/controllers/meals_detail_controller.dart';
import 'package:eradigital/app/modules/meals_detail/views/pages/ingredients.page.dart';
import 'package:eradigital/app/modules/meals_detail/views/pages/main.page.dart';
import 'package:eradigital/core/constants/color.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TabAbsensiView extends GetxController with SingleGetTickerProviderMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'LEFT'),
    Tab(text: 'RIGHT'),
  ];

  late TabController controller;

  @override
  void onInit() {
    super.onInit();
    controller = TabController(vsync: this, length: myTabs.length);
  }

  @override
  void onClose() {
    controller.dispose();
    super.onClose();
  }
}

class MealsDetailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MealsDetailController mealsDetailController = Get.find<MealsDetailController>();
    var params = Get.arguments;
    var id = params['id'];
    mealsDetailController.getDetailMeals(id);

    final TabAbsensiView _tabx = Get.put(TabAbsensiView());

    return Theme(
      data: ThemeData(
        primaryColor: info,
      ),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          titleSpacing: 0.0,
          title: Transform(
            transform: Matrix4.translationValues(0.0, 0.0, 0.0),
            child: Obx(
              () {
                return Text(
                  mealsDetailController.detailMeals.length > 0 ? mealsDetailController.detailMeals[0]['strMeal'] : '',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                );
              },
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              color: Colors.white, // Tab Bar color change
              child: TabBar(
                controller: _tabx.controller,
                unselectedLabelColor: Colors.black,
                labelColor: Colors.black,
                indicatorWeight: 2,
                indicatorColor: info_75,
                tabs: <Widget>[
                  Tab(
                    text: "Detail",
                  ),
                  Tab(
                    text: "Ingredients",
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: TabBarView(
                physics: BouncingScrollPhysics(),
                controller: _tabx.controller,
                children: <Widget>[
                  MainAbsensiPage(),
                  IngredientsPage(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
