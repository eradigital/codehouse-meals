import 'dart:math';

import 'package:eradigital/app/modules/meals_detail/controllers/meals_detail_controller.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IngredientsPage extends StatefulWidget {
  const IngredientsPage({Key? key}) : super(key: key);

  @override
  _IngredientsPageState createState() => _IngredientsPageState();
}

class _IngredientsPageState extends State<IngredientsPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Ingredient',
                style: CustomFontStyle().headingThree(Colors.pink),
              ),
              Text(
                'Measure',
                style: CustomFontStyle().headingFour(Colors.pink),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: Container(
              child: ListIngredients(),
            ),
          ),
        ],
      ),
    );
  }
}

class ListIngredients extends StatelessWidget {
  const ListIngredients({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MealsDetailController controller = Get.find<MealsDetailController>();

    return Obx(
      () {
        List allMeasure = [];
        List allIngredient = [];
        var data =
            controller.detailMeals.length > 0 ? controller.detailMeals[0] : {};
        data.forEach((k, v) {
          if (k.length > 10 && v != null && v.length > 0) {
            bool isMeasure = k.substring(0, max(0, 10)) == 'strMeasure';
            bool isIngredient = k.substring(0, max(0, 10)) == 'strIngredi';
            if (isMeasure) {
              allMeasure.add(v);
            }
            if (isIngredient) {
              allIngredient.add(v);
            }
          }
        });

        if (allIngredient.length > 0 && allMeasure.length > 0) {
          return ListView.separated(
            reverse: false,
            itemCount: allIngredient.length,
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(allIngredient[index]),
                    Text(allMeasure[index]),
                  ],
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Container(
                height: 2,
                color: Colors.pink,
              );
            },
          );
        }
        return Container();
      },
    );
  }
}
