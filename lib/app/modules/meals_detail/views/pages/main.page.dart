import 'package:eradigital/app/modules/meals_detail/views/sections/content_main.section.dart';
import 'package:eradigital/app/modules/meals_detail/views/sections/header_main.section.dart';
import 'package:eradigital/app/modules/meals_detail/views/widgets/card_item_meals.widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainAbsensiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var params = Get.arguments;

    return Scaffold(
      backgroundColor: Colors.pink,
      body: Stack(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                HeadSection(),
                ContentSection(),
              ],
            ),
          ),
          CardImageMeals(params: params),
        ],
      ),
    );
  }
}


