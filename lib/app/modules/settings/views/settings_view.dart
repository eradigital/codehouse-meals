import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SettingsController controller = Get.put(SettingsController());
    return Scaffold(
        body: ListView(
      children: [
        ElevatedButton(
          onPressed: () {
            controller.increment();
          },
          child: Text("Add"),
        ),
        Container(
          height: 400,
          color: Colors.green,
          child: Center(
            child: Obx(() => Text(controller.count.value.toString())),
          ),
        ),
        Container(
          height: 400,
          color: Colors.orange,
          child: Center(
            child: Obx(() => Text(controller.count.value.toString())),
          ),
        ),
        Container(
          height: 400,
          color: Colors.blue,
          child: Center(
            child: Obx(() => Text(controller.count.value.toString())),
          ),
        ),
      ],
    ));
  }
}
