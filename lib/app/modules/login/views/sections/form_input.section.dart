import 'package:eradigital/app/modules/login/controllers/login_controller.dart';
import 'package:eradigital/app/modules/widgets/form/text_input.widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FormInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LoginController controller = Get.find<LoginController>();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          TextInput(
            label: "Username",
            placeholder: 'Input username di sini',
            controller: controller.emailController,
            onSaved: (value) {
              controller.username = value!;
            },
            validator: (value) {
              return controller.validateUsername(value!);
            },
            obscureText: false,
            keyboardType: TextInputType.name,
          ),
          SizedBox(height: 20),
          TextInput(
            label: "Password",
            placeholder: 'Input password di sini',
            controller: controller.passwordController,
            onSaved: (value) {
              controller.password = value!;
            },
            validator: (value) {
              return controller.validatePassword(value!);
            },
            obscureText: true,
            keyboardType: TextInputType.visiblePassword,
            onFieldSubmitted: (_) {
              controller.doLogin();
            },
          ),
        ],
      ),
    );
  }
}
