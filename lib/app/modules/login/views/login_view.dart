import 'package:eradigital/app/modules/login/controllers/login_controller.dart';
import 'package:eradigital/app/modules/login/views/sections/form_input.section.dart';
import 'package:eradigital/app/modules/login/views/widgets/app_bar.widget.dart';
import 'package:eradigital/app/modules/login/views/widgets/sosmed_auth.widget.dart';
import 'package:eradigital/app/modules/login/views/widgets/utility_login.widget.dart';
import 'package:eradigital/app/modules/widgets/buttons/rounded.widget.dart';
import 'package:eradigital/core/constants/color.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: Get.height,
          width: Get.width,
          child: Stack(
            children: [
              Container(
                child: AppBarWidget(),
              ),
              Form(
                key: controller.loginFormKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: [
                    SizedBox(
                      height: 250,
                    ),
                    Container(
                      height: Get.height - 250,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FormInput(),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: UtilityLoginWidget(),
                          ),
                          Container(
                            child: const SosmedAuthWidget(),
                          ),
                          Container(
                            margin: EdgeInsets.all(20),
                            child: RoundedButton(
                              text: 'LOGIN',
                              onPress: () {
                                controller.doLogin();
                              },
                              color: info,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
