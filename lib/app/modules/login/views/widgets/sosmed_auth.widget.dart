import 'package:flutter/material.dart';

class SosmedAuthWidget extends StatelessWidget {
  const SosmedAuthWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text("Or Sign In With"),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 60,
                padding: EdgeInsets.symmetric(vertical: 5),
                decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 1),
                        blurRadius: 6.0)
                  ],
                  color: Colors.white,
                  borderRadius: new BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: Container(
                    width: 22,
                    height: 22,
                    child: Image(
                      image: AssetImage("assets/images/icons/google.png"),
                      fit: BoxFit.fill,
                      repeat: ImageRepeat.noRepeat,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Container(
                width: 60,
                padding: EdgeInsets.symmetric(vertical: 5),
                decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 1),
                        blurRadius: 6.0)
                  ],
                  color: Colors.white,
                  borderRadius: new BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: Container(
                    width: 22,
                    height: 22,
                    child: Image(
                      image: AssetImage("assets/images/icons/facebook.png"),
                      fit: BoxFit.fill,
                      repeat: ImageRepeat.noRepeat,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
