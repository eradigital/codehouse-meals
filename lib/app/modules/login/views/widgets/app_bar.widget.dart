import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(350);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Container(
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.only(bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage("assets/images/ob-001.png"),
                        fit: BoxFit.fill,
                        height: 250,
                        repeat: ImageRepeat.noRepeat,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class AppClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path clippedPath = Path();

    clippedPath.moveTo(size.width * 0.30, 0);
    clippedPath.lineTo(size.width * 1, 0);
    clippedPath.quadraticBezierTo(
        size.width * 1, size.height * 0.75, size.width * 1, size.height);
    clippedPath.quadraticBezierTo(
        size.width * 0.20, size.height * 0.87, size.width * 0.20, 0);
    clippedPath.close();

    return clippedPath;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
