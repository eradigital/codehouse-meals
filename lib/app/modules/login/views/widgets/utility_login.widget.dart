

import 'package:eradigital/app/modules/login/controllers/login_controller.dart';
import 'package:eradigital/app/modules/widgets/form/check_box.widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UtilityLoginWidget extends StatelessWidget {
  const UtilityLoginWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LoginController controller = Get.find<LoginController>();
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CheckBox(
            checkFn: (bool remember) {
              controller.doRemember(remember);
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          Text("Ingat Saya"),
          Spacer(),
          GestureDetector(
            child: Text("Lupa Password"),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}