import 'package:eradigital/core/utils/snackbar.util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  late TextEditingController emailController;
  late TextEditingController passwordController;
  var username = '';
  var password = '';

  @override
  void onInit() {
    super.onInit();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  String? validateUsername(String value) {
    if (value.length < 1) {
      return "Username harus di isi";
    }
    return null;
  }

  String? validatePassword(String value) {
    if (value.length < 1) {
      return "Password harus di isi";
    }
    return null;
  }

  Future<void> doLogin() async {
    final isValid = loginFormKey.currentState!.validate();
    if (!isValid) {
      return SnackbarUtil.showInfo(message: 'Mohon lengkapi data form apa saja, hanya untuk formalitas !');
    }

    loginFormKey.currentState!.save();
    
    Get.offAllNamed('/main-page');
  }

  Future<void> doRemember(remember) async {
    if (remember) {
      print("remember");
    } else {
      print("no remember");
    }
  }
}
