import 'package:eradigital/app/modules/home/views/sections/header.section.dart';
import 'package:eradigital/app/modules/home/views/sections/list_categories.section.dart';
import 'package:eradigital/app/modules/home/views/sections/list_meals.section.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.getAllListMeals();
    controller.getAllListCategories();
    return HomeContent();
  }
}

class HomeContent extends StatefulWidget {
  const HomeContent({
    Key? key,
  }) : super(key: key);
  @override
  _HomeContentState createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> with AutomaticKeepAliveClientMixin {
  final HomeController controller = Get.find<HomeController>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        HeaderHome(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text(
            'Category',
            style: CustomFontStyle().headingFour(Colors.black),
          ),
        ),
        Container(
          height: 80,
          child: Obx(() => controller.isLoadingCategories.value
              ? Center(
                  child: Container(
                    child: CircularProgressIndicator(),
                  ),
                )
              : ListCategoriesPage()),
        ),
        Expanded(
          child: Container(
            child: Obx(
              () => controller.isLoadingMeals.value
                  ? Center(
                      child: Container(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : ListMealsPage(),
            ),
          ),
        ),
      ],
    );
  }
}
