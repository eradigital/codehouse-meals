import 'package:cached_network_image/cached_network_image.dart';
import 'package:eradigital/app/modules/home/controllers/home_controller.dart';
import 'package:eradigital/core/animations/touchable_opacity.animate.dart';
import 'package:eradigital/core/constants/color.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListMealsPage extends StatefulWidget {
  ListMealsPage({Key? key}) : super(key: key);

  @override
  _ListMealsPageState createState() => _ListMealsPageState();
}

class _ListMealsPageState extends State<ListMealsPage> {
  HomeController controller = Get.find<HomeController>();

  int page = 1;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  GlobalKey _contentKey = GlobalKey();
  GlobalKey _refresherKey = GlobalKey();

  void _onRefresh() async {
    await Future.delayed(Duration(seconds: 2));
    // var list = await fetctPosts(page: 1); get new page data from api
    // data.clear();
    // data.addAll(list)

    setState(() {
      _refreshController.refreshCompleted();
    });
  }

  void _onLoading() async {
    await Future.delayed(Duration(seconds: 2));
    // page++;
    // var list = await fetctPosts(page: 1); get new page data from api
    // data.addAll(list);
    setState(() {
      _refreshController.loadComplete();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        List<dynamic> meals = controller.listMeals;

        return Container(
          child: RefreshConfiguration.copyAncestor(
            enableLoadingWhenFailed: true,
            context: context,
            child: SmartRefresher(
              key: _refresherKey,
              controller: _refreshController,
              enablePullUp: true,
              child: ListView.builder(
                key: _contentKey,
                itemExtent: 60.0,
                itemCount: meals.length,
                itemBuilder: (context, index) {
                  return RenderItemMeals(
                    index: index,
                  );
                },
              ),
              physics: BouncingScrollPhysics(),
              footer: ClassicFooter(
                loadStyle: LoadStyle.ShowWhenLoading,
              ),
              onRefresh: _onRefresh,
              onLoading: _onLoading,
            ),
            headerBuilder: () => WaterDropMaterialHeader(
              backgroundColor: info,
            ),
            footerTriggerDistance: 30,
          ),
        );
      },
    );
  }
}

class RenderItemMeals extends StatelessWidget {
  RenderItemMeals({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;
  final HomeController controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {

    return Obx(
      () => TouchableOpacity(
        onTap: () {
          Get.toNamed('/meals-detail', arguments: {
            'id': controller.listMeals[index]['idMeal'],
            'image': controller.listMeals[index]['strMealThumb']
          });
        },
        child: Card(
          child: Center(
            child: ListTile(
              leading: Hero(
                tag: "${controller.listMeals[index]['idMeal']}",
                child: Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: CachedNetworkImage(
                    key: UniqueKey(),
                    height: 40,
                    width: 40,
                    maxHeightDiskCache: 40,
                    maxWidthDiskCache: 40,
                    fit: BoxFit.cover,
                    imageUrl: controller.listMeals[index]['strMealThumb'],
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) =>
                            CircularProgressIndicator(
                                value: downloadProgress.progress),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              title: Text(controller.listMeals[index]['strMeal']),
              trailing: Icon(Icons.more_vert),
            ),
          ),
        ),
      ),
    );
  }
}
