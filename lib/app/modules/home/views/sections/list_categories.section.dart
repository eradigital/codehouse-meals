import 'package:cached_network_image/cached_network_image.dart';
import 'package:eradigital/app/modules/home/controllers/home_controller.dart';
import 'package:eradigital/app/modules/widgets/containers/shadow.widget.dart';
import 'package:eradigital/core/animations/touchable_opacity.animate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListCategoriesPage extends StatefulWidget {
  ListCategoriesPage({Key? key}) : super(key: key);

  @override
  _ListCategoriesPageState createState() => _ListCategoriesPageState();
}

class _ListCategoriesPageState extends State<ListCategoriesPage> {
  HomeController controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return ItemCategories(categories: controller.listCategories, controller: controller);
  }
}

class ItemCategories extends StatelessWidget {
  const ItemCategories({
    Key? key,
    required this.categories,
    required this.controller,
  }) : super(key: key);

  final List categories;
  final HomeController controller;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.builder(
        reverse: false,
        itemCount: categories.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Column(
            children: [
              RenderItemCategory(
                controller: controller,
                categories: categories,
                index: index,
              )
            ],
          );
        },
      ),
    );
  }
}

class RenderItemCategory extends StatelessWidget {
  const RenderItemCategory({
    Key? key,
    required this.controller,
    required this.categories,
    required this.index,
  }) : super(key: key);

  final HomeController controller;
  final List categories;
  final index;

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () {
        controller.setCategory(categories[index]['strCategory']);
      },
      child: Column(
        children: [
          ShadowContainer(
            color: Colors.white,
            height: 50,
            width: 50,
            margin: EdgeInsets.only(
              right: 15,
              top: 5,
              left: index == 0 ? 15 : 0,
            ),
            child: CachedNetworkImage(
              key: UniqueKey(),
              width: 40,
              height: 40,
              maxHeightDiskCache: 40,
              maxWidthDiskCache: 40,
              imageUrl: categories[index]['strCategoryThumb'],
              progressIndicatorBuilder: (context, url, downloadProgress) =>
                  CircularProgressIndicator(value: downloadProgress.progress),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Obx(
            () => Container(
              height: 3,
              width: 50,
              color:
                  categories[index]['strCategory'] == controller.category.value
                      ? Colors.pink
                      : Colors.white,
              margin: EdgeInsets.only(
                right: 15,
                top: 5,
                left: index == 0 ? 15 : 0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
