import 'package:eradigital/app/modules/home/controllers/home_controller.dart';
import 'package:eradigital/app/modules/widgets/buttons/circle.widget.dart';
import 'package:eradigital/app/modules/widgets/form/text_input.widget.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:eradigital/core/translate/home.translate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HeaderHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.put(HomeController());

    return Stack(
      children: [
        ClipPath(
          clipper: AppClipper(),
          child: Container(
            height: 240,
            color: Colors.pink,
          ),
        ),
        Positioned(
          top: 30,
          left: 20,
          child: Container(
            width: Get.width - 40,
            child: Text(
              HomeTranslate.appBar,
              style: CustomFontStyle().headingTwo(Colors.black),
            ),
          ),
        ),
        Positioned(
          top: 70,
          left: 20,
          child: Text(
            HomeTranslate.findWords,
            style: CustomFontStyle().textTwo(Colors.black),
          ),
        ),
        Positioned(
          top: 100,
          child: Container(
            width: Get.width - 40,
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Form(
                    key: controller.searchFormKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Container(
                      child: TextInput(
                        label: 'Search',
                        isLabel: false,
                        placeholder: 'Please enter keyword',
                        controller: controller.searchController,
                        onSaved: (value) {
                          controller.search = value!;
                        },
                        validator: (value) {},
                        obscureText: false,
                        keyboardType: TextInputType.visiblePassword,
                        onFieldSubmitted: (_) {
                          controller.doSeaching();
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10, top: 7),
                  child: CircleButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.blue,
                    ),
                    onPress: () {
                      controller.doSeaching();
                    },
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

class AppClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path clippedPath = Path();
    clippedPath.moveTo(0, 0);
    clippedPath.quadraticBezierTo(0, size.height * 0.42, 0, size.height * 0.7);
    clippedPath.cubicTo(size.width * 0.20, size.height * 0.9, size.width * 0.80,
        size.height * 0.9, size.width, size.height * 0.7);
    clippedPath.quadraticBezierTo(size.width, size.height * 0.7, size.width, 0);
    clippedPath.lineTo(0, 0);
    clippedPath.close();
    return clippedPath;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
