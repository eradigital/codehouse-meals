import 'package:eradigital/core/constants/storage.constants.dart';
import 'package:eradigital/core/utils/snackbar.util.dart';
import 'package:eradigital/domain/categories/categories.repository.dart';
import 'package:eradigital/domain/meals/meals.repository.dart';
import 'package:eradigital/models/categories/categories_response.model.dart';
import 'package:eradigital/models/meals/meals_response.model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:logger/logger.dart';

class HomeController extends GetxController {
  final GlobalKey<FormState> searchFormKey = GlobalKey<FormState>();
  late TextEditingController searchController;
  final MealsRepository meals = MealsRepository();
  final listMeals = <dynamic>[].obs;
  final CategoriesRepository categories = CategoriesRepository();
  final listCategories = <dynamic>[].obs;
  final category = ''.obs;
  final box = GetStorage();
  final isLoadingMeals = false.obs;
  final isLoadingCategories = false.obs;

  var search = '';
  @override
  void onInit() {
    ever(category, (_) => getAllListMeals());
    super.onInit();
    searchController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  setLoadingMeals() {
    isLoadingMeals.value = !isLoadingMeals.value;
  }

  setLoadingCategories() {
    isLoadingCategories.value = !isLoadingCategories.value;
  }

  setCategory(cat) {
    category.value = cat;
  }

  Future<void> doSeaching() async {
    searchFormKey.currentState!.save();
    if (search.length > 0) {
      findByName(search);
    } else {
      return SnackbarUtil.showInfo(message: 'Mohon masukan kata pencarian');
    }
  }

  getAllListMeals() {
    try {
      setLoadingMeals();
      var dataStorage = box.read(StorageConstants.MEALS);
      if (dataStorage != null) {
        listMeals.assignAll(dataStorage);
      }
      meals.getAll(category.value).then((MealsResponse result) {
        if (dataStorage.toString() == result.meals.toString()) {
          return;
        }
        box.write(StorageConstants.MEALS, result.meals);
        listMeals.assignAll(result.meals);
      }).catchError((error) {
        Logger().e(error);
      });
    } catch (error) {
      Logger().e(error);
    } finally {
      setLoadingMeals();
    }
  }

  getAllListCategories() {
    try {
      setLoadingCategories();
      var dataStorage = box.read(StorageConstants.CATEGORIES);
      if (dataStorage != null) {
        listCategories.assignAll(dataStorage);
      }
      categories.getAll().then((CategoriesResponse result) {
        if (dataStorage.toString() == result.categories.toString()) {
          return;
        }
        box.write(StorageConstants.CATEGORIES, result.categories);
        listCategories.assignAll(result.categories);
      }).catchError((error) {
        Logger().e(error);
      });
    } catch (error) {
      Logger().e(error);
    } finally {
      setLoadingCategories();
    }
  }

  Future<void> findByName(keyword) async {
    meals.findByName(keyword).then((MealsResponse result) {
      listMeals.assignAll(result.meals);
    }).catchError((error) {
      Logger().e(error);
    });
  }
}
