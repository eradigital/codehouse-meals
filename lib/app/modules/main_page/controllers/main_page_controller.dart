import 'package:eradigital/core/services/theme.service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class MainPageController extends GetxController {
  final isLoadingLenguage = false.obs;
  final isLoadingTheme = false.obs;
  final currentIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void changePage(int index) {
    currentIndex.value = index;
  }

  void setToIndonesia() {
    try {
      isLoadingLenguage.value = true;
      var locale = Locale('id', 'ID');
      Get.updateLocale(locale);
    } catch (error) {
      Logger().e(error);
    } finally {
      isLoadingLenguage.value = false;
    }
  }

  void setToEnglish() {
    try {
      isLoadingLenguage.value = true;
      var locale = Locale('en', 'US');
      Get.updateLocale(locale);
    } catch (error) {
      Logger().e(error);
    } finally {
      isLoadingLenguage.value = false;
    }
  }

  void setTheme() async {
    try {
      isLoadingTheme.value = true;
      ThemeService().changeThemeMode();
    } catch (error) {
      Logger().e(error);
    } finally {
      isLoadingTheme.value = false;
    }
  }
}
