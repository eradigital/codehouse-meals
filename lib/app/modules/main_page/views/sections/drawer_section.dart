import 'package:eradigital/app/modules/main_page/views/widgets/dialog_menu_settings.widget.dart';
import 'package:eradigital/core/constants/color.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DrawerSection extends StatelessWidget {
  const DrawerSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Column(
                children: [
                  Stack(
                    children: <Widget>[
                      UserAccountsDrawerHeader(
                        accountName: Text(
                          'Tedi Lesmana',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        accountEmail: const Text('tedi.lesmana0123@gmail.com',
                            style: TextStyle(fontSize: 10)),
                        currentAccountPicture: GestureDetector(
                          child: new CircleAvatar(
                            backgroundColor: Colors.white,
                            child: Icon(Icons.person, color: Colors.black),
                          ),
                        ),
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Colors.pink, info_25],
                            begin: Alignment(0.0, 0.0),
                            end: Alignment(-1.0, 3.0),
                          ),
                        ),
                        onDetailsPressed: () => {},
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: ClipPath(
                          clipper: AppClipper(),
                          child: Container(
                            height: 100.0,
                            width: 100.0,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  Colors.purple,
                                  Colors.blue,
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      Get.offAllNamed('/login');
                    },
                    child: const ListTile(
                        title: Text(
                          'Logout',
                          style: TextStyle(color: Colors.black),
                        ),
                        leading: Icon(Icons.logout, color: Colors.pink)),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Divider(),
                  InkWell(
                    onTap: () {
                      Get.dialog(
                        DialogMenuSettings(),
                      );
                    },
                    child: const ListTile(
                      title: Text(
                        'Settings',
                        style: TextStyle(color: Colors.black),
                      ),
                      leading: Icon(Icons.settings, color: Colors.pink),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed('/about');
                    },
                    child: const ListTile(
                      title: Text(
                        'About',
                        style: TextStyle(color: Colors.black),
                      ),
                      leading: Icon(Icons.help, color: Colors.pink),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AppClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path clippedPath = Path();

    clippedPath.moveTo(size.width * 0.30, 0);
    clippedPath.lineTo(size.width * 1, 0);
    clippedPath.quadraticBezierTo(
        size.width * 1, size.height * 0.75, size.width * 1, size.height);
    clippedPath.quadraticBezierTo(
        size.width * 0.20, size.height * 0.87, size.width * 0.20, 0);
    clippedPath.close();

    return clippedPath;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
