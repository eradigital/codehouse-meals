import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:eradigital/app/modules/favorite/views/favorite_view.dart';
import 'package:eradigital/app/modules/home/views/home_view.dart';
import 'package:eradigital/app/modules/main_page/views/sections/drawer_section.dart';
import 'package:eradigital/app/modules/main_page/controllers/main_page_controller.dart';
import 'package:eradigital/app/modules/messages/views/messages_view.dart';
import 'package:eradigital/app/modules/settings/views/settings_view.dart';
import 'package:eradigital/core/services/theme.service.dart';
import 'package:eradigital/core/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:showcaseview/showcaseview.dart';

class MainPageView extends GetView<MainPageController> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey _one = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return RefreshConfiguration(
      headerBuilder: () =>
          WaterDropHeader(), // Configure the default header indicator. If you have the same header indicator for each page, you need to set this
      footerBuilder: () =>
          ClassicFooter(), // Configure default bottom indicator
      headerTriggerDistance: 80.0, //
      maxOverScrollExtent:
          100, //The maximum dragging range of the head. Set this property if a rush out of the view area occurs
      maxUnderScrollExtent: 0, // Maximum dragging range at the bottom
      enableScrollWhenRefreshCompleted:
          true, //This property is incompatible with PageView and TabBarView. If you need TabBarView to slide left and right, you need to set it to true.
      enableLoadingWhenFailed:
          true, //In the case of load failure, users can still trigger more loads by gesture pull-up.
      hideFooterWhenNotFull:
          false, // Disable pull-up to load more functionality when Viewport is less than one screen
      enableBallisticLoad: true,
      child: MaterialApp(
        theme: Themes().lightTheme,
        darkTheme: Themes().darkTheme,
        themeMode: ThemeService().getThemeMode(),
        debugShowCheckedModeBanner: false,
        home: ShowCaseWidget(
          builder: Builder(
            builder: (context) {
              return MainContentPage(scaffoldKey: _scaffoldKey, one: _one);
            },
          ),
        ),
      ),
    );
  }
}

class MainContentPage extends StatefulWidget {
  const MainContentPage({
    Key? key,
    required GlobalKey<ScaffoldState> scaffoldKey,
    required GlobalKey<State<StatefulWidget>> one,
  })  : _scaffoldKey = scaffoldKey,
        _one = one,
        super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey;
  final GlobalKey<State<StatefulWidget>> _one;

  @override
  _MainContentPageState createState() => _MainContentPageState();
}

class _MainContentPageState extends State<MainContentPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(
        (_) => ShowCaseWidget.of(context)!.startShowCase([widget._one]));
  }

  MainPageController controller = Get.put(MainPageController());

  static List<Widget> _widgetOptions = <Widget>[
    HomeView(key: PageStorageKey('Home Page')),
    FavoriteView(key: PageStorageKey('Favorite Page')),
    MessagesView(key: PageStorageKey('Messages Page')),
    SettingsView(key: PageStorageKey('Settings Page')),
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget._scaffoldKey,
      appBar: AppBar(
        title: Text('Meals House'),
        leading: Showcase(
          key: widget._one,
          title: 'Menu',
          description: 'Click disini untuk membuka menu setting dan tentang aplikasi',
          child: IconButton(
            icon: SvgPicture.asset("assets/images/icons/menu.svg"),
            onPressed: () => widget._scaffoldKey.currentState!.openDrawer(),
          ),
        ),
      ),
      drawer: const DrawerSection(),
      // body: HomeView(),
      body: Obx(
            () => PageStorage(
              child: _widgetOptions.elementAt(controller.currentIndex.value),
              bucket: bucket,
            ),
          ),
          bottomNavigationBar: Obx(
            () => BottomNavyBar(
              selectedIndex: controller.currentIndex.value,
              showElevation: true,
              itemCornerRadius: 24,
              curve: Curves.easeIn,
              onItemSelected: (index) {
                controller.changePage(index);
              },
              items: <BottomNavyBarItem>[
                BottomNavyBarItem(
                  icon: Icon(Icons.apps),
                  title: Text('Home'),
                  activeColor: Colors.red,
                  textAlign: TextAlign.center, 
                ),
                BottomNavyBarItem(
                  icon: Icon(Icons.favorite),
                  title: Text('Favorites'),
                  activeColor: Colors.purpleAccent,
                  textAlign: TextAlign.center,
                ),
                BottomNavyBarItem(
                  icon: Icon(Icons.message),
                  title: Text(
                    'Messages',
                  ),
                  activeColor: Colors.pink,
                  textAlign: TextAlign.center,
                ),
                BottomNavyBarItem(
                  icon: Icon(Icons.settings),
                  title: Text('Settings'),
                  activeColor: Colors.blue,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
    );
  }
}
