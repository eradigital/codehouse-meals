import 'package:eradigital/app/modules/main_page/controllers/main_page_controller.dart';
import 'package:eradigital/app/modules/widgets/dialogs/empty_dialog.widget.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogMenuSettings extends StatelessWidget {
  const DialogMenuSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainPageController controller = Get.put(MainPageController());
    return EmptyDialogBox(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  'Settings',
                  style: CustomFontStyle().headingThree(Colors.black),
                ),
              ),
            ),
            Text(
              'Lenguage',
              style: CustomFontStyle().headingFour(Colors.black),
            ),
            Container(
              color: Colors.pink,
              height: 2,
              margin: EdgeInsets.symmetric(vertical: 5),
            ),
            InkWell(
              onTap: () {
                Get.back();
                controller.setToIndonesia();
              },
              child: const ListTile(
                title: Text(
                  'Indonesia',
                  style: TextStyle(color: Colors.black),
                ),
                leading: Icon(Icons.flag, color: Colors.pink),
              ),
            ),
            InkWell(
              onTap: () {
                Get.back();
                controller.setToEnglish();
              },
              child: const ListTile(
                title: Text(
                  'English',
                  style: TextStyle(color: Colors.black),
                ),
                leading: Icon(Icons.flag, color: Colors.pink),
              ),
            ),
            Text(
              'Theme',
              style: CustomFontStyle().headingFour(Colors.black),
            ),
            Container(
              color: Colors.pink,
              height: 2,
              margin: EdgeInsets.symmetric(vertical: 5),
            ),
            InkWell(
              onTap: () {
                Get.back();
                controller.setTheme();
              },
              child: const ListTile(
                title: Text(
                  'Change Theme',
                  style: TextStyle(color: Colors.black),
                ),
                leading: Icon(Icons.screenshot, color: Colors.pink),
              ),
            ),
            Obx(
              () {
                return !controller.isLoadingTheme.value
                    ? Container()
                    : Center(
                        child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      );
              },
            )
          ],
        ),
      ),
    );
  }
}