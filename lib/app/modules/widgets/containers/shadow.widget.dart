import 'package:flutter/material.dart';

/// Shadow container di gunakan untuk membuat card
class ShadowContainer extends StatelessWidget {
  /// [child] **variable** untuk menamung *widget* `child`
  final Widget child;

  /// [height] **variable** untuk menamung *tinggi* `container`
  final double? height;

  /// [width] **variable** untuk menamung *lebar* `container`
  final double? width;

  /// [color] **variable** untuk menamung *widget* `color`
  final Color? color;

  /// [margin] **variable** untuk menamung *jarak luar* `container`
  final EdgeInsets? margin;

  /// [child] **variable** untuk menamung *widget* `child`
  /// [height] **variable** untuk menamung *tinggi* `container`
  /// [color] **variable** untuk menamung *widget* `color`
  /// [width] **variable** untuk menamung *lebar* `container`
  /// [margin] **variable** untuk menamung *jarak luar* `container`
  ///
  /// ```
  /// ShadowContainer(
  ///       color: Colors.white,
  ///       height: 50,
  ///       width: 50,
  ///       margin: EdgeInsets.only(
  ///           right: 15, top: 5, left: index == 0 ? 15 : 0),
  ///       child: CachedNetworkImage(
  ///         imageUrl: categories[index]['strCategoryThumb'],
  ///         progressIndicatorBuilder: (context, url, downloadProgress) =>
  ///             CircularProgressIndicator(
  ///                 value: downloadProgress.progress),
  ///         errorWidget: (context, url, error) => Icon(Icons.error),
  ///       ),
  ///     )
  /// ```
  const ShadowContainer({
    Key? key,
    required this.child,
    this.height,
    this.width,
    this.margin,
    this.color = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      margin: margin,
      padding: EdgeInsets.symmetric(vertical: 5),
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black26, offset: Offset(0, 1), blurRadius: 6.0)
        ],
        color: color,
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: child,
    );
  }
}
