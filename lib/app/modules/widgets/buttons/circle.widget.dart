import 'package:eradigital/core/animations/touchable_opacity.animate.dart';
import 'package:flutter/material.dart';

/// Class Circle Button digunakan untuk membuat button bulat
class CircleButton extends StatelessWidget {
  /// [icon] **variable** untuk menamung *widget* `icon`
  final Icon icon;

  /// [onPress] di gunakan untuk menjalankan sebuah `function`
  final Function onPress;

  /// [color] **variable** untuk menamung *widget* `color`
  final Color color;

  /// [icon] **variable** untuk menamung *widget* `icon`
  /// [onPress] di gunakan untuk menjalankan sebuah `function`
  /// [color] **variable** untuk menamung *widget* `color`
  ///
  /// ```
  /// CircleButton(
  ///icon: Icon(
  ///  Icons.search,
  ///color: Colors.blue,
  ///),
  ///onPress: () {
  /// controller.doSeaching();
  /// },
  ///color: Colors.white,
  /// )
  /// ```
  CircleButton({
    required this.icon,
    required this.onPress,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: onPress,
      child: Container(
        height: 40.0,
        width: 40.0,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Center(
          child: icon,
        ),
      ),
    );
  }
}
