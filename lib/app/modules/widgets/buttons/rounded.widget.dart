import 'package:eradigital/core/animations/touchable_opacity.animate.dart';
import 'package:flutter/material.dart';

/// Class Rounded Button digunakan untuk membuat button dengan ujung curve/tidak lancip
class RoundedButton extends StatelessWidget {
  /// [text] **variable** untuk menamung *widget* `icon`
  final String text;
  /// [onPress] di gunakan untuk menjalankan sebuah `function`
  final Function onPress;
  /// [color] **variable** untuk menamung *widget* `color`
  final Color color;

  /// [text] **variable** untuk menamung *widget* `icon`
  /// [onPress] di gunakan untuk menjalankan sebuah `function`
  /// [color] **variable** untuk menamung *widget* `color`
  ///
  /// ```
  ///RoundedButton(
  ///  text: 'LOGIN',
  ///  onPress: () {
  /// controller.doLogin();
  ///   },
  /// color: info,
  /// )
  /// ```
  RoundedButton({
    required this.text,
    required this.onPress,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return TouchableOpacity(
      onTap: () => onPress(),
      child: Container(
        height: 40.0,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }
}
