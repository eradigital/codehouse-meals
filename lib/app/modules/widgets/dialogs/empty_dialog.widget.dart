import 'package:flutter/material.dart';

class Constants {
  Constants._();
  static const double padding = 20;
}

/// Dialog pop up tanpa content/masih kosong
class EmptyDialogBox extends StatefulWidget {
  final Widget child;

  /// [child] **variable** untuk menampung *widget* `child`
  /// 
  /// ```
  /// EmptyDialogBox(
  ///   child: Container(),
  ///  )
  /// ```
  const EmptyDialogBox({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  _EmptyDialogBoxState createState() => _EmptyDialogBoxState();
}

class _EmptyDialogBoxState extends State<EmptyDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                left: Constants.padding,
                top: Constants.padding,
                right: Constants.padding,
                bottom: Constants.padding),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Constants.padding),
            ),
            child: widget.child,
          ),
        ),
      ],
    );
  }
}
