import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/// Widget untuk membuat kolom input
class TextInput extends StatelessWidget {
  /// [label] **variable** untuk menamung *nama* `input`
  final String label;

  /// [placeholder] **variable** untuk menamung *petunjuk* `input`
  final String placeholder;

  /// [controller] **variable** untuk controller *widget* `input`
  final TextEditingController controller;

  /// [onFieldSubmitted] **variabel** untuk mentrigger *Function* `submit pada keyboard`
  final Function? onFieldSubmitted;

  /// [isLabel] **variable** untuk memenuculkan / menghilangkan *label* `input`
  final bool? isLabel;

  /// [onSaved] **variabel** untuk mentrigger *Function* `simpan data input`
  final Function onSaved;

  /// [validator] **variabel** untuk mentrigger *Function* `validasi data input`
  final Function validator;

  /// [obscureText] **variable** untuk memunculkan/menyembunyikan apa yang di input
  final bool obscureText;

  /// [keyboardType] **variable** untuk memilih jenis keyboard *yang di gunakan saat* `input`
  final TextInputType keyboardType;

  /// [label] **variable** untuk menamung *nama* `input`
  /// [placeholder] **variable** untuk menamung *petunjuk* `input`
  /// [controller] **variable** untuk controller *widget* `input`
  /// [isLabel] **variable** untuk memenuculkan / menghilangkan *label* `input`
  /// [onFieldSubmitted] **variabel** untuk mentrigger *Function* `submit pada keyboard`
  /// [onSaved] **variabel** untuk mentrigger *Function* `simpan data input`
  /// [validator] **variabel** untuk mentrigger *Function* `validasi data input`
  /// [obscureText] **variable** untuk memunculkan/menyembunyikan apa yang di input
  /// [keyboardType] **variable** untuk memilih jenis keyboard *yang di gunakan saat* `input`
  ///
  /// ```
  ///       TextInput(
  ///          label: "Password",
  ///          placeholder: 'Input password di sini',
  ///          controller: controller.passwordController,
  ///          onSaved: (value) {
  ///            controller.password = value!;
  ///          },
  ///          validator: (value) {
  ///            return controller.validatePassword(value!);
  ///          },
  ///          obscureText: true,
  ///          keyboardType: TextInputType.visiblePassword,
  ///          onFieldSubmitted: (_) {
  ///            controller.doLogin();
  ///          },
  ///        )
  /// ```
  ///
  TextInput({
    required this.label,
    required this.placeholder,
    required this.controller,
    this.isLabel = true,
    this.onFieldSubmitted,
    required this.onSaved,
    required this.validator,
    required this.obscureText,
    required this.keyboardType,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isLabel!
            ? Text(
                '$label : ',
                style: TextStyle(
                  fontSize: 14,
                ),
              )
            : Container(),
        Stack(
          children: [
            Container(
              height: 45,
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: new BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 1),
                      blurRadius: 6.0)
                ],
                color: Colors.white,
                borderRadius: new BorderRadius.circular(10.0),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                keyboardType: keyboardType,
                obscureText: obscureText,
                onFieldSubmitted: (String string) {
                  onFieldSubmitted!(string);
                },
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: "$placeholder",
                  hintStyle: TextStyle(fontSize: 13),
                  contentPadding: const EdgeInsets.only(left: 20.0),
                ),
                onSaved: (value) => onSaved(value),
                validator: (value) => validator(value),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
