import 'package:flutter/material.dart';

/// Widget checkbox input untuk checklist atau unchecklist input
class CheckBox extends StatefulWidget {
  final Function(bool remember) checkFn;

  /// [checkFn] **variabel** untuk mentrigger *Function* `checkFn`
  ///
  /// ```
  /// CheckBox(
  ///   checkFn: (bool remember) {
  ///     controller.doRemember(remember);
  ///   },
  /// )
  /// ```
  CheckBox({required this.checkFn});

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool rememberMe = false;

  void _onRememberMeChanged(bool newValue) => setState(() {
        rememberMe = newValue;
        widget.checkFn(newValue);
      });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      clipBehavior: Clip.hardEdge,
      borderRadius: BorderRadius.all(Radius.circular(5)),
      child: SizedBox(
        width: Checkbox.width,
        height: Checkbox.width,
        child: Container(
          decoration: new BoxDecoration(
            border: Border.all(
              width: 1,
            ),
            borderRadius: new BorderRadius.circular(5),
          ),
          child: Theme(
            data: ThemeData(
              unselectedWidgetColor: Colors.transparent,
            ),
            child: Checkbox(
              value: rememberMe,
              onChanged: (bool? value) {
                if (value != null) {
                  _onRememberMeChanged(value);
                }
              },
              materialTapTargetSize: MaterialTapTargetSize.padded,
            ),
          ),
        ),
      ),
    );
  }
}
