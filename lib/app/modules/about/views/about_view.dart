import 'package:eradigital/app/modules/widgets/containers/shadow.widget.dart';
import 'package:eradigital/core/constants/font.constant.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/about_controller.dart';

class AboutView extends GetView<AboutController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Apps'),
        centerTitle: true,
      ),
      body: Container(
        height: Get.height - Get.statusBarHeight,
        width: Get.width,
        child: SingleChildScrollView(
          child: ShadowContainer(
            width: double.maxFinite,
            margin: EdgeInsets.all(20),
            child: Container(
              margin: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      height: 80,
                      width: 80,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: Image(
                          image: AssetImage("assets/images/me.jpeg"),
                          fit: BoxFit.fill,
                          repeat: ImageRepeat.noRepeat,
                        ),
                      ),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 1),
                              blurRadius: 6.0)
                        ],
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(50.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(
                      "Profile Developer",
                      style: CustomFontStyle().headingFour(Colors.black),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text('Name : Tedi Lesmana'),
                  Text(
                      'Address : Kp. Ciputat RT01/RW13 Kel. Andir Kec. Baleendar, bandung'),
                  Text('Proffesion : Mobile Developer for 3 years'),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Gambaran Umum',
                    style: CustomFontStyle().headingFour(Colors.black),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                      'Ini adalah aplikasi panduan memasak lengkap berbagai masakan dari berbagai negara untuk membantu ibu memasak di rumah'),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Kelebihan',
                    style: CustomFontStyle().headingFour(Colors.black),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Aplikasi ini dibangun dengan menggunakan bahasa pemrograman Dart menggunakan framework Flutter, untuk struktur pemrograman menggunakan arsitektur Resocoder dan dibuat mengikuti OOP untuk memudahkan dalam maintenance dan memudahkan rekan satu tim dalam memahami struktur program. Selain itu untuk memudahkan kerjasama tim telah di berikan dokumentasi untuk setiap widgetnya. Untuk mempercepat akses data pada aplikasi ini menggunakan get storage untuk penyimpanana data local dan berperan sebagai cache untuk imagenya sendiri menggunakna chace network image',
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
