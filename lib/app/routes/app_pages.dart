import 'package:get/get.dart';

import 'package:eradigital/app/modules/about/bindings/about_binding.dart';
import 'package:eradigital/app/modules/about/views/about_view.dart';
import 'package:eradigital/app/modules/favorite/bindings/favorite_binding.dart';
import 'package:eradigital/app/modules/favorite/views/favorite_view.dart';
import 'package:eradigital/app/modules/home/bindings/home_binding.dart';
import 'package:eradigital/app/modules/home/views/home_view.dart';
import 'package:eradigital/app/modules/login/bindings/login_binding.dart';
import 'package:eradigital/app/modules/login/views/login_view.dart';
import 'package:eradigital/app/modules/main_page/bindings/main_page_binding.dart';
import 'package:eradigital/app/modules/main_page/views/main_page_view.dart';
import 'package:eradigital/app/modules/meals_detail/bindings/meals_detail_binding.dart';
import 'package:eradigital/app/modules/meals_detail/views/meals_detail_view.dart';
import 'package:eradigital/app/modules/messages/bindings/messages_binding.dart';
import 'package:eradigital/app/modules/messages/views/messages_view.dart';
import 'package:eradigital/app/modules/onboarding/bindings/onboarding_binding.dart';
import 'package:eradigital/app/modules/onboarding/views/onboarding_view.dart';
import 'package:eradigital/app/modules/settings/bindings/settings_binding.dart';
import 'package:eradigital/app/modules/settings/views/settings_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.ONBOARDING;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.ONBOARDING,
      page: () => OnboardingView(),
      binding: OnboardingBinding(),
    ),
    GetPage(
      name: _Paths.MAIN_PAGE,
      page: () => MainPageView(),
      binding: MainPageBinding(),
    ),
    GetPage(
      name: _Paths.MEALS_DETAIL,
      page: () => MealsDetailView(),
      binding: MealsDetailBinding(),
    ),
    GetPage(
      name: _Paths.ABOUT,
      page: () => AboutView(),
      binding: AboutBinding(),
    ),
    GetPage(
      name: _Paths.FAVORITE,
      page: () => FavoriteView(),
      binding: FavoriteBinding(),
    ),
    GetPage(
      name: _Paths.MESSAGES,
      page: () => MessagesView(),
      binding: MessagesBinding(),
    ),
    GetPage(
      name: _Paths.SETTINGS,
      page: () => SettingsView(),
      binding: SettingsBinding(),
    ),
  ];
}
