part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const ONBOARDING = _Paths.ONBOARDING;
  static const MAIN_PAGE = _Paths.MAIN_PAGE;
  static const MEALS_DETAIL = _Paths.MEALS_DETAIL;
  static const ABOUT = _Paths.ABOUT;
  static const FAVORITE = _Paths.FAVORITE;
  static const MESSAGES = _Paths.MESSAGES;
  static const SETTINGS = _Paths.SETTINGS;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const ONBOARDING = '/onboarding';
  static const MAIN_PAGE = '/main-page';
  static const MEALS_DETAIL = '/meals-detail';
  static const ABOUT = '/about';
  static const FAVORITE = '/favorite';
  static const MESSAGES = '/messages';
  static const SETTINGS = '/settings';
}
