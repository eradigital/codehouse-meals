import 'package:eradigital/core/services/api.service.dart';
import 'package:get/get.dart';

class CategoriesService extends GetxService {
  ServiceApi serviceApi = ServiceApi();

  Future<dynamic> getAll() async {
    final _response = await serviceApi.getService(url: '/categories.php');
    return _response;
  }

  Future<dynamic> getId(id) async {
    return this;
  }

  Future<dynamic> delete(id) async {
    return this;
  }

  Future<dynamic> add(data) async {
    return this;
  }

  Future<dynamic> edit(data) async {
    return this;
  }
}
