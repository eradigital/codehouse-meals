import 'package:eradigital/core/services/api.service.dart';
import 'package:get/get.dart';

class MealsService extends GetxService {
  ServiceApi serviceApi = ServiceApi(); 

  Future<dynamic> getAll(cat) async {
    if (cat.length > 0) {
      final _response =
          await serviceApi.getService(url: '/filter.php?c=$cat&s=b');
      return _response;
    } else {
      final _response = await serviceApi.getService(url: '/search.php?f=b');
      return _response;
    }
  }

  Future<dynamic> getId(id) async {
    final _response = await serviceApi.getService(url: '/lookup.php?i=' + id);
    return _response;
  }

  Future<dynamic> findByName(keyword) async {
    final _response =
        await serviceApi.getService(url: '/search.php?s=' + keyword);
    return _response;
  }

  Future<dynamic> add(data) async {
    return this;
  }

  Future<dynamic> edit(data) async {
    return this;
  }
}
