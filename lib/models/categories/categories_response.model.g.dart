// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: implicit_dynamic_parameter, non_constant_identifier_names

part of 'categories_response.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategoriesResponse _$_$_CategoriesResponseFromJson(
    Map<String, dynamic> json) {
  return _$_CategoriesResponse(
    json['categories'] as List<dynamic>,
  );
}

Map<String, dynamic> _$_$_CategoriesResponseToJson(
        _$_CategoriesResponse instance) =>
    <String, dynamic>{
      'categories': instance.categories,
    };
