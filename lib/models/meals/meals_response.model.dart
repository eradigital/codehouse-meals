import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'meals_response.model.freezed.dart';
part 'meals_response.model.g.dart';

@freezed
abstract class MealsResponse with _$MealsResponse {
  const factory MealsResponse(
    List meals,
  ) = _MealsResponse;
  factory MealsResponse.fromJson(Map<String, dynamic> json) =>
      _$MealsResponseFromJson(json);
}