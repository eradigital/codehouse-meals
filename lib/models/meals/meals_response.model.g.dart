// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: implicit_dynamic_parameter, non_constant_identifier_names

part of 'meals_response.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MealsResponse _$_$_MealsResponseFromJson(Map<String, dynamic> json) {
  return _$_MealsResponse(
    json['meals'] as List<dynamic>,
  );
}

Map<String, dynamic> _$_$_MealsResponseToJson(_$_MealsResponse instance) =>
    <String, dynamic>{
      'meals': instance.meals,
    };
