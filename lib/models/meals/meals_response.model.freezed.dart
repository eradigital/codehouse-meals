// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'meals_response.model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MealsResponse _$MealsResponseFromJson(Map<String, dynamic> json) {
  return _MealsResponse.fromJson(json);
}

/// @nodoc
class _$MealsResponseTearOff {
  const _$MealsResponseTearOff();

  _MealsResponse call(List<dynamic> meals) {
    return _MealsResponse(
      meals,
    );
  }

  MealsResponse fromJson(Map<String, Object> json) {
    return MealsResponse.fromJson(json);
  }
}

/// @nodoc
const $MealsResponse = _$MealsResponseTearOff();

/// @nodoc
mixin _$MealsResponse {
  List<dynamic> get meals => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MealsResponseCopyWith<MealsResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MealsResponseCopyWith<$Res> {
  factory $MealsResponseCopyWith(
          MealsResponse value, $Res Function(MealsResponse) then) =
      _$MealsResponseCopyWithImpl<$Res>;
  $Res call({List<dynamic> meals});
}

/// @nodoc
class _$MealsResponseCopyWithImpl<$Res>
    implements $MealsResponseCopyWith<$Res> {
  _$MealsResponseCopyWithImpl(this._value, this._then);

  final MealsResponse _value;
  // ignore: unused_field
  final $Res Function(MealsResponse) _then;

  @override
  $Res call({
    Object? meals = freezed,
  }) {
    return _then(_value.copyWith(
      meals: meals == freezed
          ? _value.meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
abstract class _$MealsResponseCopyWith<$Res>
    implements $MealsResponseCopyWith<$Res> {
  factory _$MealsResponseCopyWith(
          _MealsResponse value, $Res Function(_MealsResponse) then) =
      __$MealsResponseCopyWithImpl<$Res>;
  @override
  $Res call({List<dynamic> meals});
}

/// @nodoc
class __$MealsResponseCopyWithImpl<$Res>
    extends _$MealsResponseCopyWithImpl<$Res>
    implements _$MealsResponseCopyWith<$Res> {
  __$MealsResponseCopyWithImpl(
      _MealsResponse _value, $Res Function(_MealsResponse) _then)
      : super(_value, (v) => _then(v as _MealsResponse));

  @override
  _MealsResponse get _value => super._value as _MealsResponse;

  @override
  $Res call({
    Object? meals = freezed,
  }) {
    return _then(_MealsResponse(
      meals == freezed
          ? _value.meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MealsResponse with DiagnosticableTreeMixin implements _MealsResponse {
  const _$_MealsResponse(this.meals);

  factory _$_MealsResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_MealsResponseFromJson(json);

  @override
  final List<dynamic> meals;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MealsResponse(meals: $meals)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MealsResponse'))
      ..add(DiagnosticsProperty('meals', meals));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MealsResponse &&
            (identical(other.meals, meals) ||
                const DeepCollectionEquality().equals(other.meals, meals)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(meals);

  @JsonKey(ignore: true)
  @override
  _$MealsResponseCopyWith<_MealsResponse> get copyWith =>
      __$MealsResponseCopyWithImpl<_MealsResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MealsResponseToJson(this);
  }
}

abstract class _MealsResponse implements MealsResponse {
  const factory _MealsResponse(List<dynamic> meals) = _$_MealsResponse;

  factory _MealsResponse.fromJson(Map<String, dynamic> json) =
      _$_MealsResponse.fromJson;

  @override
  List<dynamic> get meals => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MealsResponseCopyWith<_MealsResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
