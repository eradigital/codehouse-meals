import 'package:dio/dio.dart';
import 'package:dartz/dartz.dart';
import 'package:eradigital/config.dart';
import 'package:eradigital/core/utils/handle_exception.util.dart';

class ServiceApi {
  Dio _dio = Dio();
  HandleException exceptionService = HandleException();
  final baseUrl = ConfigEnvironments.getEnvironments()['url'].toString();
  final token = 'KEY-TOKEN';

  Future<Either<String, dynamic>> getService({required url}) async {
    Response _response;
    try {
      _response = await _dio.get(
        baseUrl + url,
        options: Options(
          contentType: 'multipart/form-data',
          headers: {
            'Accept': "application/json",
            'Authorization': 'Bearer $token',
          },
        ),
      );
      
      return right(_response);
    } on DioError catch (dioError) {
      String errorMessage = exceptionService.handleDioError(dioError);
      return left(errorMessage);
    } catch (e) {
      return left(e.toString());
    }
  }

  Future<Either<String, dynamic>> postService(
      {required url, required data}) async {
    Response _response;
    try {
      _response = await _dio.post(
        baseUrl + url,
        data: data,
        options: Options(
          contentType: 'multipart/form-data',
          headers: {
            'Accept': "application/json",
            'Authorization': 'Bearer $token',
          },
        ),
      );

      return right(_response);
    } on DioError catch (dioError) {
      String errorMessage = exceptionService.handleDioError(dioError);
      return left(errorMessage);
    }
  }

  Future<Either<String, dynamic>> updateService(
      {required url, required data}) async {
    Response _response;
    try {
      _response = await _dio.put(
        baseUrl + url,
        data: data,
        options: Options(
          contentType: 'multipart/form-data',
          headers: {
            'Accept': "application/json",
            'Authorization': 'Bearer $token',
          },
        ),
      );

      return right(_response);
    } on DioError catch (dioError) {
      String errorMessage = exceptionService.handleDioError(dioError);
      return left(errorMessage);
    }
  }

  Future<Either<String, dynamic>> deleteService(
      {required url, required data}) async {
    Response _response;
    try {
      _response = await _dio.delete(
        baseUrl + url,
        data: data,
        options: Options(
          contentType: 'multipart/form-data',
          headers: {
            'Accept': "application/json",
            'Authorization': 'Bearer $token',
          },
        ),
      );

      return right(_response);
    } on DioError catch (dioError) {
      String errorMessage = exceptionService.handleDioError(dioError);
      return left(errorMessage);
    }
  }
}
