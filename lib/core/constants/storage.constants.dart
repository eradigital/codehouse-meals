abstract class StorageConstants {
  static const String MEALS = 'meals';
  static const String CATEGORIES = 'categories';
}
