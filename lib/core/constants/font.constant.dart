import 'package:flutter/material.dart';

class CustomFontStyle {
  TextStyle headingOne(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 40,
      fontWeight: FontWeight.w700,
    );
  }

  TextStyle headingTwo(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 30,
      fontWeight: FontWeight.w700,
    );
  }

  TextStyle headingThree(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 20,
      fontWeight: FontWeight.w700,
    );
  }

  TextStyle headingFour(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 15,
      fontWeight: FontWeight.w700,
    );
  }

  TextStyle textOne(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 12,
    );
  }

  TextStyle textTwo(Color color) {
    return TextStyle(
      fontFamily: 'Roboto',
      color: color,
      fontSize: 14,
    );
  }
}
