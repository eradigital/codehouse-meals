import 'package:flutter/material.dart';

class Themes {
  final lightTheme = ThemeData.light().copyWith(
    primaryColor: Colors.pink,
    appBarTheme: AppBarTheme(
      brightness: Brightness.light,
      textTheme: TextTheme(
        headline1: TextStyle(
          color: Colors.black,
        ),
      ),
    ),
    buttonColor: Colors.pink,
  );

  final darkTheme = ThemeData(
    primaryColor: Colors.black,
    appBarTheme: AppBarTheme(
      brightness: Brightness.dark,
      textTheme: TextTheme(
        headline1: TextStyle(color: Colors.white),
      ),
    ),
    buttonColor: Colors.green,
  );
}
