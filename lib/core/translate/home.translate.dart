import 'package:get/get.dart';

class HomeTranslate {
  static const _INITIAL = 'home';

  static const _APP_BAR = '$_INITIAL-app-bar';
  static String get appBar => _APP_BAR.tr;

  static const _FIND_WORDS = '$_INITIAL-find-words';
  static String get findWords => _FIND_WORDS.tr;

  static Map<String, String> translateEN = {
    _APP_BAR: 'Welcome',
    _FIND_WORDS: 'Find your favorite meals',
  };

  static Map<String, String> translateID = {
    _APP_BAR: 'Selamat Datang',
    _FIND_WORDS: 'Silahkan Cari Menu Favorite',
  };
}
