import 'package:get/get.dart';

import 'home.translate.dart';

class Translate extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {}..addAll(HomeTranslate.translateEN),
        'id_ID': {}..addAll(HomeTranslate.translateID)
      };
}
