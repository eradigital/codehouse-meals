import 'package:eradigital/config.dart';
import 'package:eradigital/core/utils/snackbar.util.dart';
import 'package:logger/logger.dart';

class DefaultException implements Exception {
  final env = ConfigEnvironments.getEnvironments()['env'];
  final String message;
  final StackTrace? stackTrace;
  DefaultException({
    this.message = 'Oops something went wrong!, please wait our service under maintanance',
    this.stackTrace,
  }) {
    bool isProduction = env != Environments.PRODUCTION;
    if(isProduction ) Logger().e(stackTrace);
    SnackbarUtil.showError(message: message);
  }

  @override
  String toString() => message;
}
