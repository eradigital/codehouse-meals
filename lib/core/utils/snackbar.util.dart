import 'package:eradigital/core/constants/color.constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SnackbarUtil {
  static void showSuccess({required String message}) {
    Get.snackbar(
      "Sukses!",
      message,
      titleText: Text(
        "SUKSES..",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(
        message,
        style: TextStyle(color: Colors.white),
      ),
      icon: const Icon(Icons.thumb_up, color: Colors.white),
      backgroundColor: success,
      shouldIconPulse: true,
      barBlur: 10,
      isDismissible: true,
      duration: Duration(seconds: 5),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    );
  }

  static void showWarning({required String message}) {
    Get.snackbar(
      "Perhatian!",
      message,
      titleText: Text(
        "PERHATIAN..",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(
        message,
        style: TextStyle(color: Colors.white),
      ),
      icon: const Icon(Icons.warning, color: Colors.white),
      backgroundColor: warning,
      shouldIconPulse: true,
      barBlur: 10,
      isDismissible: true,
      duration: Duration(seconds: 5),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    );
  }

  static void showError({required String message}) {
    Get.snackbar(
      "Error!",
      message,
      titleText: Text(
        "ERROR..",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(
        message,
        style: TextStyle(color: Colors.white),
      ),
      icon: const Icon(Icons.warning, color: Colors.white),
      backgroundColor: danger,
      shouldIconPulse: true,
      barBlur: 10,
      isDismissible: true,
      duration: Duration(seconds: 5),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    );
  }

  static void showInfo({required String message}) {
    Get.snackbar(
      "Info!",
      message,
      titleText: Text(
        "INFO..",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(
        message,
        style: TextStyle(color: Colors.white),
      ),
      icon: const Icon(Icons.warning, color: Colors.white),
      backgroundColor: info,
      shouldIconPulse: true,
      barBlur: 10,
      isDismissible: true,
      duration: Duration(seconds: 5),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    );
  }
}
