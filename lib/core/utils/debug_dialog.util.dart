import 'package:eradigital/app/modules/widgets/dialogs/custom_dialog.widget.dart';
import 'package:get/get.dart';

class DebugDialog {
  static void showMessage(String message) {
    Get.dialog(CustomDialogBox(
      descriptions: message,
      title: 'Info',
      text: 'OK',
    ));
  }
}
