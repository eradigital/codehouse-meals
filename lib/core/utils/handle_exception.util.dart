import 'package:dio/dio.dart';
import 'package:eradigital/config.dart';
import 'package:eradigital/core/exceptions/cancel.exception.dart';
import 'package:eradigital/core/exceptions/connect_time_out.exception.dart';
import 'package:eradigital/core/exceptions/default.exception.dart';
import 'package:eradigital/core/exceptions/receive_time_out.exception.dart';
import 'package:eradigital/core/exceptions/send_time_out.exception.dart';

class HandleException {
  final env = ConfigEnvironments.getEnvironments()['env'];

  String _handleError(int statusCode, dynamic error) {
    bool isProduction = env != Environments.PRODUCTION;

    switch (statusCode) {
      case 400:
        DefaultException(
            message: isProduction
                ? 'Dio Response Error : Bad request'
                : 'Bad request');
        return 'Bad request';
      case 404:
        DefaultException(
            message: isProduction
                ? 'Dio Response Error : ${error["message"]}'
                : error["message"]);
        return '${error["message"]}';
      case 500:
        DefaultException(
            message: isProduction
                ? 'Dio Response Error : Internal server error, error code'
                : 'Internal server error, error code');
        return 'Internal server error, error code';
      default:
        DefaultException(
            message: isProduction
                ? 'Dio Response Error : Oops something went wrong!, please wait our service under maintanance'
                : 'Oops something went wrong!, please wait our service under maintanance');
        return 'Oops something went wrong';
    }
  }

  String handleDioError(dioError) {
    bool isProduction = env != Environments.PRODUCTION;
    String errorMessage = dioError.toString();

    switch (dioError.type) {
      case DioErrorType.connectTimeout:
        ConnectTimeOutException(
            message: isProduction
                ? 'Dio Exception : Connection timeout with API server'
                : 'Connection timeout with API server');
        break;
      case DioErrorType.sendTimeout:
        SendTimeOutException(
            message: isProduction
                ? 'Dio Exception : Send timeout in connection with API server'
                : 'Send timeout in connection with API server');
        break;
      case DioErrorType.receiveTimeout:
        ReceiveTimeOutException(
            message: isProduction
                ? 'Dio Exception : Receive timeout in connection with API server'
                : 'Receive timeout in connection with API server');
        break;
      case DioErrorType.response:
        DefaultException(
            message: isProduction
                ? 'Dio Exception : Error response'
                : 'Error response',
            stackTrace: dioError.stackTrace);
        errorMessage = _handleError(
            dioError.response!.statusCode!, dioError.response!.data);
        break;
      case DioErrorType.cancel:
        CancelException(
            message: isProduction
                ? 'Dio Exception : Request to API server was cancelled'
                : 'Request to API server was cancelled');
        break;
      case DioErrorType.other:
        DefaultException(
            message: isProduction
                ? 'Dio Exception : Oops something went wrong!, please wait our service under maintanance'
                : 'Oops something went wrong!, please wait our service under maintanance');
        break;
    }

    return errorMessage;
  }
}
