# eradigital

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Profile Developer
- Name : Tedi Lesmana
- Address : Kp. Ciputat RT01/RW13 Kel. Andir Kec. Baleendar, bandung
- Proffesi : Mobile Developer for 3 years

## flutter run --profile --trace-skia
Menjalankan testing performance.

## Gambaran umum aplikasi
Ini adalah aplikasi panduan memasak lengkap berbagai masakan dari berbagai negara untuk membantu ibu memasak di rumah
## Deskripsi Code
- Aplikasi ini dibangun dengan menggunakan bahasa pemrograman Dart menggunakan framework Flutter, 
- Untuk struktur pemrograman menggunakan arsitektur Resocoder dan dibuat mengikuti OOP untuk memudahkan dalam maintenance dan memudahkan rekan satu tim dalam memahami struktur program. 
- Selain itu untuk memudahkan kerjasama tim telah di berikan dokumentasi untuk setiap widgetnya. 
- Untuk mempercepat akses data pada aplikasi ini menggunakan get storage untuk penyimpanana data local dan berperan sebagai cache 
- Untuk imagenya sendiri menggunakna chace network image
- State management menggunakan GetX yang memiliki performa lebih baik dari state management lainnya menurut data di beberapa artikel.
- Menggunakan Freeze dan json serialize untuk modeling object

## Home Screen
![Alt text](assets/screen-shoot/home.jpeg?raw=true "Optional Title")
## Main Detail Screen
![Alt text](assets/screen-shoot/detail-main.jpeg?raw=true "Optional Title")
## Instruction Detail Screen
![Alt text](assets/screen-shoot/detail-instruction.jpeg?raw=true "Optional Title")
## Login Screen
![Alt text](assets/screen-shoot/login.jpeg?raw=true "Optional Title")
## Drawer Menu
![Alt text](assets/screen-shoot/drawer.jpeg?raw=true "Optional Title")
## Onboarding Screen
![Alt text](assets/screen-shoot/onboarding.jpeg?raw=true "Optional Title")
## Splash Screen
![Alt text](assets/screen-shoot/splash-screen.jpeg?raw=true "Optional Title")
## Settings Screen
![Alt text](assets/screen-shoot/settings.jpeg?raw=true "Optional Title")
## About Screen
![Alt text](assets/screen-shoot/about.jpeg?raw=true "Optional Title")

